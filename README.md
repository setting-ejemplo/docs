# DOCKER #

* git clone https://setting-ejemplo@bitbucket.org/setting-ejemplo/docs.git

## Instalacion ##

### Windows ###

- Windows 10 (Hyper-h)
    - https://download.docker.com/win/stable/InstallDocker.msi
- Windows 7 (Virtualbox
    - https://download.docker.com/win/stable/DockerToolbox.exe

### Debian/Ubuntu ###
* apt-get update
* apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common -y
* curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
* add-apt-repository \
   	"deb [arch=amd64] https://download.docker.com/linux/debian \
  	 $(lsb_release -cs) \
     stable"
* curl -L https://github.com/docker/compose/releases/download/1.13.0/docker-compose-'uname -s'-'uname -m' > /usr/local/bin/docker-compose
* chmod +x /usr/local/bin/docker-compose
* apt-get update && apt-get install docker-ce -y
* groupadd docker
* usermod -aG docker $DEFAULT_USER

## Ejecucion ##
#### Wildfly ####
* docker run
	--detach
	--restart always
	--name wildfly-container
	--network setting
	--ip 172.13.1.30
	--publish 8080:8080
	--publish 9990:9990
	settingejemplo/wildfly

### Extras ###
